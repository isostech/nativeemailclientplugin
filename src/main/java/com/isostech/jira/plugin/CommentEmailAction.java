package com.isostech.jira.plugin;

import javax.servlet.http.*;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.IssueManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.IOException;
import com.atlassian.jira.issue.MutableIssue;

import com.atlassian.mail.server.MailServerManager;
import com.atlassian.mail.server.PopMailServer;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

public class CommentEmailAction extends JiraWebActionSupport {
	private String email = null;
	private String subject = null;

	private static String emailTo = null;
	private static String subjectPrefix = null;
	private final PluginSettingsFactory pluginSettingsFactory;

	public CommentEmailAction(PluginSettingsFactory pluginSettingsFactory) {
		super();
		this.pluginSettingsFactory = pluginSettingsFactory;
		emailTo = this.getConfigurationParam("emailTo");
		subjectPrefix = this.getConfigurationParam("subjectPrefix");
		System.out.println("Constructor: emailTo: " + emailTo
				+ " subject prefix: " + subjectPrefix);
		setEmail(emailTo);
		HttpServletRequest httpRequest = this.getHttpRequest();

		String id = httpRequest.getParameter("id");
		if (id != null) {
			Long idl = Long.parseLong(id);
			IssueManager im = ComponentAccessor.getIssueManager();
			MutableIssue issue = im.getIssueObject(idl);

			subject = "(" + issue.getKey() + ") " + issue.getSummary();
			if (subjectPrefix != null) {
				subject = subjectPrefix + " " + subject;
			}
			System.out.println("subject: " + subject);
			this.setSubject(subject);
		}

	}

	public String doExecute() throws Exception {
		if (emailTo != null) {	 
			this.saveConfigurationParam("emailTo", emailTo);
			this.saveConfigurationParam("subjectPrefix",subjectPrefix);
			return "success";
		} else {
			return "configure";
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getSubjectPrefix() {
		return subjectPrefix;
	}

	public void setSubjectPrefix(String subjectPrefix) {
		this.subjectPrefix = subjectPrefix;
	}

	private static final String PLUGIN_CONFIGURATION_PARAM = "com.isostech.jira.plugins.commentemail.config";

	public void saveConfigurationParam(String name, String value) {
		if (name != null && value != null) {

			PluginSettings pluginSettings = pluginSettingsFactory
					.createGlobalSettings();
			String key = this.PLUGIN_CONFIGURATION_PARAM + "_" + name;

			System.out.println("saving configuration param key: " + key
					+ " value: " + value);
			String existing = (String) pluginSettings.get(key);
			if (pluginSettings.get(key) == null) {
				System.out.println("saving key and value ");
				pluginSettings.put(key, value);
			} else {
				System.out.println("Found existing value: " + existing
						+ " remove. ");
				pluginSettings.remove(key);
				pluginSettings.put(key, value);
			}
		}
	}

	public String getConfigurationParam(String name) {

		PluginSettings pluginSettings = pluginSettingsFactory
				.createGlobalSettings();
		String key = this.PLUGIN_CONFIGURATION_PARAM + "_" + name;

		String value = (String) pluginSettings.get(key);

		System.out.println("lookup configuration param with key: " + key
				+ " value: " + value);

		return value;
	}

}
